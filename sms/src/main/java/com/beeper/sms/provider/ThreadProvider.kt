package com.beeper.sms.provider

import android.content.Context
import android.net.Uri
import android.provider.Telephony.*
import android.provider.Telephony.BaseMmsColumns.*
import android.telephony.PhoneNumberUtils
import androidx.core.net.toUri
import com.beeper.sms.extensions.firstOrNull
import com.beeper.sms.extensions.getLong
import com.beeper.sms.extensions.getString
import com.beeper.sms.extensions.map
import java.util.*

class ThreadProvider constructor(context: Context) {
    private val packageName = context.applicationInfo.packageName
    private val cr = context.contentResolver

    fun getRecentConversations(minTimestamp: Long): List<String> =
        cr.map(URI_THREADS, "${Sms.Conversations.DATE} > $minTimestamp AND ${ThreadsColumns.MESSAGE_COUNT} > 0") {
            getChatGuid(it.getLong(ThreadsColumns._ID))
        }

    fun getMessagesAfter(thread: Long, timestampSeconds: Long): List<Pair<Long, Boolean>> =
        getMessagesAfter(thread, timestampSeconds * 1000, false)
            .plus(getMessagesAfter(thread, timestampSeconds, true))
            .sortedBy { it.first }
            .map { Pair(it.second, it.third) }

    fun getMmsMessagesAfter(timestampSeconds: Long): List<Long> =
        cr.map(
            Mms.CONTENT_URI,
            where = and(
                "${Mms.CREATOR} != '$packageName'",
                or(
                    "$MESSAGE_BOX = $MESSAGE_BOX_OUTBOX",
                    "$MESSAGE_BOX = $MESSAGE_BOX_SENT"
                ),
                "${Mms.DATE} > $timestampSeconds"
            ),
            order = "${Sms.Conversations.DATE} DESC"
        ) {
            it.getLong(Sms.Conversations._ID)
        }

    fun getRecentMessages(thread: Long, limit: Int): List<Pair<Long, Boolean>> =
        getRecentMessages(thread, limit, false)
            .plus(getRecentMessages(thread, limit, true))
            .sortedBy { it.first }
            .takeLast(limit)
            .map { Pair(it.second, it.third) }

    fun getChatGuid(thread: Long): String? =
        getAddresses(thread)
            ?.mapNotNull { addr -> getPhoneNumber(addr) }
            ?.takeIf { it.isNotEmpty() }
            ?.chatGuid

    private fun getRecentMessages(thread: Long, limit: Int, mms: Boolean) =
        getMessages(thread, mms.isMms, "LIMIT $limit")

    private fun getMessagesAfter(thread: Long, timestamp: Long, mms: Boolean) =
        getMessages(thread, "${mms.isMms} AND ${Sms.Conversations.DATE} > $timestamp")

    private fun getMessages(thread: Long, where: String, limit: String = "") =
        cr.map(
            Uri.withAppendedPath(MmsSms.CONTENT_CONVERSATIONS_URI, thread.toString()),
            where = where,
            projection = PROJECTION,
            order = "${Sms.Conversations.DATE} DESC $limit"
        ) {
            val mms = it.getString(MmsSms.TYPE_DISCRIMINATOR_COLUMN) == "mms"
            Triple(
                it.getLong(Sms.Conversations.DATE) * if (mms) 1000 else 1,
                it.getLong(Sms.Conversations._ID),
                mms
            )
        }

    private fun getAddresses(thread: Long): List<String>? =
        cr.firstOrNull(URI_THREADS, "${Mms._ID} = $thread") {
            it.getString(ThreadsColumns.RECIPIENT_IDS)?.split(" ")
        }

    private fun getPhoneNumber(recipient: String): String? =
        cr.firstOrNull(URI_ADDRESSES, "${Mms._ID} = $recipient") {
            it.getString(Mms.Addr.ADDRESS)
        }

    companion object {
        val URI_THREADS = "${MmsSms.CONTENT_CONVERSATIONS_URI}?simple=true".toUri()
        private val URI_ADDRESSES = "${MmsSms.CONTENT_URI}/canonical-addresses".toUri()
        private val PROJECTION = arrayOf(
            Sms.Conversations._ID,
            Sms.Conversations.DATE,
            MmsSms.TYPE_DISCRIMINATOR_COLUMN,
        )

        val Boolean.isMms: String
            get() = "${MmsSms.TYPE_DISCRIMINATOR_COLUMN} ${if (this) "=" else "!="} 'mms'"

        internal val String.normalize: String
            get() =
                PhoneNumberUtils
                    .formatNumberToE164(this, Locale.getDefault().country)
                    ?.takeIf { it != this }
                    ?: filterNot { it.isWhitespace() }

        val String.chatGuid: String
            get() = listOf(this).chatGuid

        val List<String>.chatGuid: String
            get() = "SMS;${if (size == 1) "-" else "+"};${joinToString(" ") { it.normalize }}"

        private fun and(vararg criteria: String) = joinCriteria(" AND ", criteria)

        private fun or(vararg criteria: String) = joinCriteria(" OR ", criteria)

        private fun joinCriteria(separator: String, criteria: Array<out String>) =
            if (criteria.size == 1) {
                criteria.first()
            } else {
                criteria.joinToString(separator, "(", ")")
            }
    }
}